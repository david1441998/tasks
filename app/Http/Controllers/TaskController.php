<?php
use Illuminate\Support\Facades\Response;
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Task;
use App\User;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $id=Auth::id();
        $tasks = Task::all() ;
        return view('tasks.index', ['tasks' => $tasks]);
    }
    public function my()
    {
        //
        $id=Auth::id();
        $tasks = User::find($id)->tasks;
        return view('tasks.index', ['tasks' => $tasks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view ('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
                     $task = new Task();
                         $id=Auth::id();
                      $task->title = $request->title;
                        $task->user_id = $id;
                        $task->save();
                        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $task = Task::find($id);
        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

        //
        
 

    ////
    public function update(Request $request, $id)
    {
        //
      //  $task = Task::find($id);
          //     $task -> update($request->all());
               
               $todo = Todo::find($id);
               if(!$todo->user->id == Auth::id()) return(redirect('todos'));
               $todo -> update($request->except(['_token']));
       return redirect('tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to create todos..");
        } 

        $task = Task::find($id);
              $task->delete();
              return redirect('tasks');
    }
    public function done($id)
    {
        //only if this todo belongs to user 
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to mark tasks as dome..");
         }          
        $task = Task::findOrFail($id);            
        $task->status = 1; 
        $task->save();
        return redirect('tasks');    
    }    
}
