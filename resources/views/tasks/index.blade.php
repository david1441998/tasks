@extends('layouts.app')
@section('content')
@if (Request::is('tasks'))  

<h2><a href="{{action('TaskController@my')}}">My Tasks</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">All Tasks</a></h2>

@endif


<h1>This is your task list</h1>
<ul>
   @foreach($tasks as $task)

   <li>
      id: {{$task->id}} title:{{$task->title}} 
      <a href= "{{route('tasks.edit', $task->id )}}"> edit </a>
   
      @if ($task->status == 0)
            @can('admin')
             <a href="{{route('done', $task->id)}}">Mark As done</a>
            @endcan 
            @else
            Done!
            @endif



       
   </li>
   @endforeach
   <a href="{{route('tasks.create')}}">Create New task </a>

</ul>
@endsection


