<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert(
            [
        [
            'name' => 'Jack',
           'email' =>'a@a.com',
           'password' =>Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            'role'=>'admin',
       ],
       ]);
    }
}
//ה.	בנו seeder שמכניס משתמש אחד למערכת בעל הרשאת 
//admin (משתמש זה אמור לקבל id=1) המייל של המשתמש a@a.com והססמא 12345678 (אל תשכחו להצפין את הסיסמא)