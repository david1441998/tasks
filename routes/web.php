<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('tasks', 'TaskController')->middleware('auth');
///tasks/completed/{task}
//Route::patch('/sta', 'TaskController@button' )->name('statu');
Route::get('tasks/done/{id}', 'TaskController@done')->name('done');
Route::get('/my', 'TaskController@my' );